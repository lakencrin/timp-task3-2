from flask import Flask, request
import json

app = Flask(__name__)

infos = dict()

def validateBody(olddata):
    data = {}
    data['userName'] = olddata['userName']
    oldcomputerInfo = olddata['computerInfo']
    computerInfo = {}
    computerInfo['computerName'] = oldcomputerInfo['computerName']
    computerInfo['domainName'] = oldcomputerInfo['domainName']
    data['computerInfo'] = computerInfo
    data['ram'] = olddata['ram']
    data['processorName'] = olddata['processorName']
    data['osName'] = olddata['osName']
    return data

@app.route('/infos/<string:mac>', methods=['POST'])
def index(mac):
    data = validateBody(json.loads(request.data))
    infos[mac] = data
    json.dumps(data, indent=4)
    return 'Info for %s was updated'%mac

@app.route('/infos', methods=['GET'])
def infosList():
    return json.dumps(infos, indent=4)

app.run(debug=True, port=7777)