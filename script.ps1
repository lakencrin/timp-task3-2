ForEach ($row in (getmac))
{
    $result = $row -match '(([\d\w]{2}\-){5}[\d\w]{2})'
    if ($result -eq 'True') {
        $mac_address = $Matches[0]  
    }
}

$ram = 0
ForEach ($row in ((get-wmiobject -class "win32_physicalmemory").Capacity)) {
    $ram += $row / 1024
}

$username = $env:Username
$computerName = (hostname)
$domainName = (Get-WmiObject -Namespace root\cimv2 -Class Win32_ComputerSystem).GetPropertyValue("Domain")
$osnameraw = ((Get-WMIObject win32_operatingsystem).name)

$osname = $osnameraw.Split('|')[0]

$processorName = (Get-WmiObject Win32_Processor).GetPropertyValue("Name")
Write-Output $mac_address, $username, $computerName, $domainName, $ram, $processorName, $osname

$data = @"
{
    "userName": "$userName",
    "computerInfo": {
        "computerName" : "$computerName",
        "domainName" : "$domainName"
    },
    "ram" : "$ram",
    "processorName" : "$processorName",
    "osName" : "$osName"
}
"@
$url = 'http://localhost:7777/infos/' + $mac_address

Invoke-RestMethod -Method POST -Uri $url -ContentType 'application/json' -Body $data